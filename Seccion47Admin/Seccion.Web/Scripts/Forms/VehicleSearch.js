﻿$(document).ready(function () {
    $("#resultado").hide();
});
$('#buscarInfo').click(function () {
    var chassi = $("#txtChassi").val();
    var plate = $("#txtPlate").val();
    var nameUser = $("#txtUserName").val();
    $("#hdChassi").val(chassi);
    $("#hdPlate").val(plate);
    $("#hdNameUser").val(nameUser);
    if (nameUser === "" && chassi === "" && plate === "") {
        NotifierMessage('Nenhum filtro de pesquisa foi inserido!!!', 'warning', "Você deve inserir pelo menos um filtro para poder pesquisar informações.");
        return;
    }
    if (nameUser !== "" && chassi === "" && plate === "") {
        NotifierMessage('Sem resultados!!!', 'info', "Para realizar pesquisas por nome, é necessário que você insira o chassi ou a placa do veículo a partir da qual os detalhes da informação serão obtidos.");
        return;
    }

    $.ajax({
        type: "Post",
        url: "/" + lang + "/VehicularRequest/GetInfoVehicle/",
        dataType: "json",
        data: { Chassis: chassi, Plate: plate, NameUser: nameUser },
        success: function (data) {
            if (data === 'null') {
                $("#hdChassi").val("");
                $("#hdPlate").val("");
                $("#hdNameUser").val("");
                $("#resultado").hide();
                NotifierMessage('Requer informações adicionais!!!', 'info', "Nenhuma informação foi encontrada, com os dados inseridos");
                return;
            }
            $("#resultado").show();
            $("#txtChassi").val("");
            $("#txtPlate").val("");
            nameUser = $("#txtUserName").val("");
            var myArray = $.parseJSON(data);
            $.each(myArray, function (key, value) {
                if ((key === "PlateDate" || key === "ValidationCHNDate" || key === "InvoiceDate" || key === "RequestDate" || key === "DateAvailable" ||
                    key === "DateRealDelivered" || key === "DepartureDate") && (value !== "" || value !== "null")) {
                    value = FormatDateShort(value);
                }
                $("#" + key).val(value);
            });
        },
        error: function (errorMessage) {
            NotifierMessage('Advertência!', 'warning', "Warning");
        }
    });
});

function DownloadReportVehicle() {
    var chassi = $("#hdChassi").val();
    var plate = $("#hdPlate").val();
    var nameUser = $("#hdNameUser").val();
    if ((nameUser === "" && chassi === "" && plate === "") || (nameUser !== "" && chassi === "" && plate === "")) {
        NotifierMessage('sem informação', 'warning', "nenhuma informação encontrada para baixar.");
        return;
    }
    $.ajax({
        type: "GET",
        url: "/" + lang + "/VehicularRequest/CrateReportInfoVehicle/",
        dataType: "json",
        data: { Chassis: chassi, Plate: plate, NameUser: nameUser },
        success: function (data) {
            if (data === 'null') {
                $("#resultado").hide();
                NotifierMessage('sem informação!!!', 'info', "nenhuma informação encontrada para baixar");
                return;
            }
            window.location = '/VehicularRequest/DownloadSingle?file=' + data;
        },
        error: function (errorMessage) {
            NotifierMessage('Advertência!', 'warning', "Warning");
        }
    });
}
﻿$(document).ready(function () {
    var form = $(".steps-validation").show();
    var FirstRequestBeginDate = $("#txtFirstRequestBeginDate").val();
    var FirstRequestEndDate = $("#txtFirstRequestEndDate").val();

    $('[data-toggle="tooltip"]').tooltip();   

    $("#ddlFirstRequestVehicleUpdate, #ddlSecondRequestVehicleUpdate").select2({});

    $("#txtFirstRequestBeginDate").daterangepicker({
        timePicker: true,
        singleDatePicker: true,
        startDate: FirstRequestBeginDate,
        locale:
            {
                format: 'DD-MM-YYYY h:mm A'
            }
    });

    $("#txtFirstRequestEndDate").daterangepicker({
        timePicker: true,
        singleDatePicker: true,
        startDate: FirstRequestEndDate,
        minDate: FirstRequestBeginDate,
        locale:
            {
                format: 'DD-MM-YYYY h:mm A'
            }
    });

    $("select").on("select2:close", function (e) {
        $(this).valid();
    });

    $(".nav-tabs a[data-toggle=tab]").on("click", function (e) {
        if ($(this).hasClass("disabled")) {
            e.preventDefault();
            return false;
        }
    });

    $("#txtFirstRequestBeginDate").change(function () {
        var newDate = $(this).val();
        $("#txtFirstRequestEndDate").daterangepicker({
            timePicker: true,
            singleDatePicker: true,
            startDate: FirstRequestEndDate,
            minDate: newDate,
            locale:
                {
                    format: 'DD-MM-YYYY h:mm A'
                }
        });
    });

    $("#txtSecondRequestBeginDate").change(function () {
        $.ajax({
            type: "POST",
            url: "/" + lang + "/VehicularRequest/CheckDateChange/",
            async: false,
            dataType: "json",
            data: { EndDate: $("#txtFirstRequestEndDate").val(), BeginDate: $("#txtSecondRequestBeginDate").val() },
            success: function (data) {
                if (data.success === true) {
                    $("#btnSaveChangeSecondRequest").removeClass("disabled");
                }
                else {
                    $("#btnSaveChangeSecondRequest").addClass("disabled");
                }
            },
            error: function (errorMessage) {
                NotifierMessage('Advertência!', 'warning', "Warning");
            }
        });
    });

    $("#ddlFirstRequestVehicleUpdate").change(function () {
        var RealVehicleID = $("#ddlFirstRequestVehicleUpdate").val();
        $("#hdFirstRequestRealVehicleID").val(RealVehicleID);
    });

    $("#ddlSecondRequestVehicleUpdate").change(function () {
        var value = $(this).val();

        $("#hdSecondRequestRealVehicleID").val(value);
        if (value !== "") {
            $("#btnSaveChangeSecondRequest").removeClass("disabled");
        }
        else {
            $("#btnSaveChangeSecondRequest").addClass("disabled");
        }
    });

    $("#btnConfirmVehicleExit").click(function () {
        form.validate().settings.ignore = ":disabled,:hidden";
        if (form.valid() === true) {
            $.ajax({
                type: "POST",
                url: "/" + lang + "/VehicularRequest/ConfirmDeliveryVehicleUser/",
                async: false,
                dataType: "json",
                data: { PoolAssignmentID: $("#PoolAssignmentID").val() },
                success: function (data) {
                    if (data.success === true) {
                        window.location.href = "/" + lang + "/VehicularRequest/GetVehiclePoolRequests?opcion=" + 1 + "&OrderNumber=''&vehicle=''";
                    }
                    else {
                        NotifierMessage('Advertência!', 'warning', data.message);
                    }
                },
                error: function (errorMessage) {
                    NotifierMessage('Advertência!', 'warning', errorMessage.ErrorMessage);
                }
            });
        }
        else {
            return false;
        }
    });

    $("#btnConfirmVehicleEntry").click(function () {
        form.validate().settings.ignore = ":disabled,:hidden";
        if (form.valid() === true) {
            $.ajax({
                type: "POST",
                url: "/" + lang + "/VehicularRequest/ConfirmationDeliveryFleet/",
                async: false,
                dataType: "json",
                data: { PoolAssignmentID: $("#PoolAssignmentID").val() },
                success: function (data) {
                    if (data.success === true) {
                        window.location.href = "/" + lang + "/VehicularRequest/GetVehiclePoolRequests?opcion=" + 1 + "&OrderNumber=''&vehicle=''";
                    }
                    else {
                        NotifierMessage('Advertência!', 'warning', data.message);
                    }
                },
                error: function (errorMessage) {
                    NotifierMessage('Advertência!', 'warning', errorMessage.ErrorMessage);
                }
            });
        }
        else {
            return false;
        }
    });

    $("#btnCancelFirstRequest").click(function () {
        CancelRequest($("#VehicularRequestID").val());
    });

    $("#btnCancelRequest").click(function () {
        CancelRequest($("#hdSecondRequestVehicularRequestID").val());
    });

    $("#btnSaveChangeFirstRequest").click(function () {
        form.validate().settings.ignore = ":disabled,:hidden";
        if (form.valid() === true) {
            $.ajax({
                type: "POST",
                url: "/" + lang + "/VehicularRequest/SaveChangeFirstRequest/",
                dataType: "json",
                data: {
                    VehicularRequestID: $("#VehicularRequestID").val(),
                    PoolAssignmentID: $("#PoolAssignmentID").val(),
                    BeginDate: $("#txtFirstRequestBeginDate").val(),
                    EndDate: $("#txtFirstRequestEndDate").val(),
                    RealVehicleID: ($("#hdFirstRequestRealVehicleID").val() === "" ? $("#RealVehicleID").val() : $("#hdFirstRequestRealVehicleID").val())
                },
                success: function (data) {
                    if (data.success === true && data.message === 'Exito') {
                        window.location.href = "/" + lang + "/VehicularRequest/GetVehiclePoolRequests?opcion=" + 2 + "&OrderNomber=''&vehicle=''";
                    }
                    else {
                        var objJson = JSON.parse(data);
                        if (objJson.success === true) {
                            var lstVehicle = objJson.lstVehicle;
                            var objPool = objJson.objPool;
                            if (objPool !== null) {
                                $('.nav-tabs li:eq(1) a').tab('show');
                                $("#ddlSecondRequestVehicleUpdate").empty();
                                $("#ddlSecondRequestVehicleUpdate").append(
                                    $('<option/>', { value: "", text: "Selecione" })
                                );
                                if (lstVehicle.length !== 0) {
                                    $.each(lstVehicle, function (index, item) {
                                        $("#ddlSecondRequestVehicleUpdate").append(
                                            $('<option/>', { value: item.Value, text: item.Text })
                                        );
                                    });
                                }
                                if (objPool.ReturnConditions === "1") {
                                    $("#btnCancelRequest").hide();
                                    $("#txtSecondRequestBeginDate").attr("disabled", "disabled");
                                    $("#ddlSecondRequestVehicleUpdate").hide();
                                }
                                $("#hdSecondRequestPoolAssignmentID").val(objPool.PoolAssignmentID);
                                $("#hdSecondRequestVehicularRequestID").val(objPool.TbVehicularRequest.VehicularRequestID);
                                $("#txtSecondRequestOrderNumber").val(objPool.TbVehicularRequest.OrderNumber);
                                $("#txtSecondRequestUserName").val(objPool.User.Name);
                                $("#txtSecondRequestChassi").val(objPool.RealVehicle.Chassis);
                                $("#txtSecondRequestVehicleAttributed").val(objPool.RealVehicle.Descripcion_Vehicule);

                                $("#txtSecondRequestBeginDate").daterangepicker({
                                    timePicker: true,
                                    singleDatePicker: true,
                                    startDate: FormatDate(objPool.TbVehicularRequest.CheckOutDate),
                                    minDate: FormatDate(objPool.TbVehicularRequest.CheckOutDate),
                                    locale:
                                        {
                                            separator: " / ",
                                            format: 'DD-MM-YYYY h:mm A',
                                            applyLabel: "Aceitar",
                                            cancelLabel: "Cancelar",
                                            monthNames: monthNames,
                                            daysOfWeek: daysOfWeek
                                        }
                                });
                                $("#txtSecondRequestEndDate").daterangepicker({
                                    timePicker: true,
                                    singleDatePicker: true,
                                    startDate: FormatDate(objPool.TbVehicularRequest.EndDate),
                                    minDate: FormatDate(objPool.TbVehicularRequest.CheckOutDate),
                                    locale:
                                        {
                                            separator: " / ",
                                            format: 'DD-MM-YYYY h:mm A',
                                            applyLabel: "Aceitar",
                                            cancelLabel: "Cancelar",
                                            monthNames: monthNames,
                                            daysOfWeek: daysOfWeek
                                        }
                                });
                            }
                        }
                    }
                },
                error: function (errorMessage) {
                    NotifierMessage('Advertência!', 'warning', errorMessage.ErrorMessage);
                }
            });
        }
        else {
            return false;
        }
    });

    $("#btnSaveChangeSecondRequest").click(function () {
        if ($(this).hasClass("disabled")) {
            e.preventDefault();
            return false;
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        if (form.valid() === true) {
            var JsonFirstRequest = {
                PoolAssignmentID: $("#PoolAssignmentID").val(),
                BeginDate: $("#txtFirstRequestBeginDate").val(),
                EndDate: $("#txtFirstRequestEndDate").val(),
                RealVehicleID: $("#hdFirstRequestRealVehicleID").val()
            };
            var JsonSecondRequest = {
                PoolAssignmentID: $("#hdSecondRequestPoolAssignmentID").val(),
                BeginDate: $("#txtSecondRequestBeginDate").val(),
                EndDate: $("#txtSecondRequestEndDate").val(),
                RealVehicleID: $("#hdSecondRequestRealVehicleID").val() 
            };
            $.ajax({
                type: "POST",
                url: "/" + lang + "/VehicularRequest/SaveChangeSecondRequest/",
                async: false,
                dataType: "json",
                data: { JsonFirstRequest: JSON.stringify(JsonFirstRequest), JsonSecondRequest: JSON.stringify(JsonSecondRequest) },
                success: function (data) {
                    if (data.success === true) {
                        window.location.href = "/" + lang + "/VehicularRequest/GetVehiclePoolRequests?opcion=" + data.opcion + "&OrderNumber=" + data.ordernumber + "&Vehicle=" + data.vehicle;
                    }
                    else {
                        NotifierMessage('Advertência!', 'warning', data.message);
                    }
                },
                error: function (errorMessage) {
                    NotifierMessage('Advertência!', 'warning', errorMessage.ErrorMessage);
                }
            });
        }
        else {
            return false;
        }
    });

    ValidatorForm("frmConfirmarFechaEntrega", "Este campo é requerido", "Sucesso", ValidateRegisterUserJson);
});

function CancelRequest(VehicularRequestID) {
    swal({
        title: 'Você tem certeza de cancelar o pedido',
        text: "Indique a Causa",
        type: 'info',
        input: 'textarea',
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: '#c3092e',
        cancelButtonColor: '#000000',
        confirmButtonText: 'Salvar',
        cancelButtonText: 'Cancelar',
        inputPlaceholder: 'Indique a Causa',
        showLoaderOnConfirm: true,
        preConfirm: function (textarea) {
            return new Promise(function (resolve) {// => {
                if (textarea === '') {
                    swal.showValidationError('Este campo é requerido');
                } resolve();
            });
        },
        allowOutsideClick: false
    }).then(function (result) {
        if (result.value) {
            var approv = { "VehicularRequestID": VehicularRequestID, "Comments": result.value };
            $.ajax({
                url: "/" + lang + "/VehicularRequest/CancelRequest/",
                type: 'POST',
                data: JSON.stringify(approv),
                contentType: "application/json; charset:utf-8",
                dataType: 'json',
            }).done(function (data) {
                if (data.success)
                    endConfirm("Exito", "/" + lang + "/VehicularRequest/GetVehiclePoolRequests?opcion=" + 3 + "&OrderNumber=''&vehicle=''");
            }).fail(function (data) {
                swal('Error', data.errorMessage);
            });
        }
    }).catch(function (error) {
        swal('Error', "Ocorreu um erro ao processar o pedido => " + error);
    });
}

function endConfirm(mensaje, url) {
    swal({
        title: 'Suceso',
        text: mensaje,
        type: 'success',
        showCancelButton: false,
        allowOutsideClick: false,
        animation: true
    }).then(function (isConfirm) {
        if (isConfirm) {
            window.location.href = url;
        }
    });
}
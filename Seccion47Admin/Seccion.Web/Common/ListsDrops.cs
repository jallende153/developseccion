﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Seccion.Web.Common
{
	/// <summary>
	/// Clase que contiene el llenado de todos los DropdawList Fijos que se muestran en la web
	/// </summary>
	public class ListsDrops
	{
		/// <summary>
		/// Lista de situcacion contratual
		/// </summary>
		/// <returns>Retorna una lista fijos</returns>
		protected internal static List<SelectListItem> GetListContractualSituation(string situacionContractual = null)
		{
			List<SelectListItem> list = new List<SelectListItem>() {
				new SelectListItem() { Text = "TS", Value = "TS" },
				new SelectListItem() { Text = "PS", Value = "PS" },
				new SelectListItem() { Text = "JU", Value = "JU" },
				new SelectListItem() { Text = "TC", Value = "TC" },
				new SelectListItem() { Text = "PC", Value = "PC" },
				new SelectListItem() { Text = "CS", Value = "CS" },
				new SelectListItem() { Text = "LQ", Value = "LQ" },
				new SelectListItem() { Text = "PM", Value = "PM" },
			};
			if(situacionContractual != null)
			{
				list.ForEach(x => { 
				    if(x.Value == situacionContractual)
					{
						x.Selected = true;
					}
				});
			}
			return list;
		}
		/// <summary>
		/// Ubicacion de trabajo
		/// </summary>
		/// <returns>Retorna una lista de ubicacion de trabajo</returns>
		protected internal static List<SelectListItem> GetListWorkLocation(string workLocation = null)
		{
			List<SelectListItem> list = new List<SelectListItem>()
			{
				   //new SelectListItem() {Text="MAR", Value="1" },
				   //new SelectListItem() {Text="TIERRA", Value="2" }
				   new SelectListItem() {Text="MAR", Value="MAR" },
				   new SelectListItem() {Text="TIERRA", Value="TIERRA" }
			};
			if(workLocation != null)
			{
				list.ForEach(x => {
				    if(x.Value == workLocation) 
					{
						x.Selected = true;
					}
				});
			}
			return list;
		}
		/// <summary>
		/// Lista de Regiones
		/// </summary>
		/// <returns>Retorna una lista de regiones</returns>
		protected internal static List<SelectListItem> GetListRegion(string region= null)
		{
			

			List<SelectListItem> list = new List<SelectListItem>() {
				new SelectListItem() { Text = "RMNE", Value="RMNE" },
				new SelectListItem() { Text = "RMSO", Value="RMSO" },
				new SelectListItem() { Text = "PERFORACION", Value="PERFORACION" },
				new SelectListItem() { Text = "COORPORATIVO", Value="COORPORATIVO" },
				new SelectListItem() { Text = "SIRHN", Value="SIRHN" },
			};

			if (region != null)
			{
				list.ForEach( x =>
				{
					if(x.Value == region)
					{
						x.Selected = true;
					}
				});
			}
			return list;
		}
		/// <summary>
		/// Numero de niveles 
		/// </summary>
		/// <returns>Retorna una lista de niveles</returns>
		protected internal static List<SelectListItem> GetListLevel(int? level = null)
		{
			//List<string> list = new List<string>();
			List<SelectListItem> list = new List<SelectListItem>();

			for (int i = 8; i <= 44; i++) //el i-- es porque, es de forma decendente
			{
				list.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString()});
			}
			if(level != null)
			{
				list.ForEach(x =>
				{
					if (x.Value == level.ToString())
					{
						x.Selected = true;
					}
				});
			}
			return list;
		}
		protected internal static List<SelectListItem> GetListGender()
		{
			List<SelectListItem> list = new List<SelectListItem>() {

							   new SelectListItem() { Text = "MUJER", Value = "1" },
							   new SelectListItem() { Text = "HOMBRE", Value= "2" }
			};
			return  list;
		}
		protected internal static List<SelectListItem> PaymentType()
		{
			List<SelectListItem> list = new List<SelectListItem>() {

							   new SelectListItem() { Text = "Efectivo", Value = "1" },
							   new SelectListItem() { Text = "Deposito Bancario", Value= "2" },
							   new SelectListItem() { Text = "Vía Nomina", Value= "3" }
			};
			return list;
		}
	}
}
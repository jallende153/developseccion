﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.GridListModel
{
	public class ListEmployeeViewModel
	{
        public int TbEmployeeId { get; set; }
        public string NumberFicha { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContractualSituation { get; set; }
        public string WorkLocation { get; set; }
        public string Rol { get; set; }
        public string Region { get; set; }
        public string Level { get; set; }
        public string DepartmentKey { get; set; }
        public int TotalEmployees { get; set; }
        public Int64 RowNum { get; set; }
    }
}
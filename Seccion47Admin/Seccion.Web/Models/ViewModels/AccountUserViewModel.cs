﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.ViewModels
{
	public class AccountUserViewModel
	{
        public int Id { get; set; }

        [Display(Name = "Nombre:")]
        public string Name { get; set; }

        //Apellido Paterno
        [Display(Name = "Apellido Paterno:")]
        public string FirstName { get; set; }

        //Apellido Materno
        [Display(Name = "Apellido Materno:")]
        public string LastName { get; set; }


        [Display(Name = "Fecha de Nacimiento:")]
        public string BirthDate { get; set; }

        [Display(Name = "Sexo:")]
        public string Gender { get; set; }

        [Display(Name = "Correo Electronico:")]
        public string Email { get; set; }


        [Display(Name = "Número de Telefonico:")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Perfil de Usuario")]
        public string Role { get; set; }

        public bool Active { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Seccion.Web.Models.ViewModels
{
    public class EmployeeVIewModel
	{
		/// <summary>
        /// ID (PK) de la clase tabla en Base de datos
        /// </summary>
        public int TbEmployeeId { get; set; }
        
        /// <summary>
        /// ID(FK) relacion con la tabla Estados
        /// </summary>
        public int TbStatesId { get; set; }
        
        /// <summary>
        /// ID(FK) relacion con la tabla Municipios
        /// </summary>
        public int TbMunicipalitiesId { get; set; }
        
        /// <summary>
        /// Numero de ficha
        /// </summary>
        [Display(Name = "Ficha:")]
        public string NumberFicha { get; set; }
        
        /// <summary>
        /// Nombre del empleado
        /// </summary>
        [Display(Name = "Nombre(s):")]
        public string Name { get; set; }
        
        /// <summary>
        /// Apelido parterno del empleado
        /// </summary>
        [Display(Name = "Apellido Paterno:")]
        public string FirstName { get; set; }
        
        /// <summary>
        /// Apelido materno del empleado
        /// </summary>
        [Display(Name = "Apellido Materno:")]
        public string LastName { get; set; }
        /// <summary>
        /// RFC del empleado
        /// </summary>
        [Display(Name = "RFC:")]
        public string RFC { get; set; }

        /// <summary>
        /// Fecha de nacimiento
        /// </summary>
        [Display(Name = "Fecha de Nacimiento:")]
        public string BirthDate { get; set; }
        /// <summary>
        /// Situacion contractual del empleado
        /// </summary>
        [Display(Name = "Situacion Contractual:")]
        public string ContractualSituation { get; set; }
        /// <summary>
        /// Ubicacion laboral del empleado
        /// </summary>
        [Display(Name = "Ubicacion Laboral:")]
        public string WorkLocation { get; set; }
        /// <summary>
        /// Rol del empleado
        /// </summary>
        [Display(Name = "Rol:")]
        public string Rol { get; set; }
        /// <summary>
        /// Region del empleado
        /// </summary>
        [Display(Name = "Region:")]
        public string Region { get; set; }
        /// <summary>
        /// Clave de departemento del empleado
        /// </summary>
        [Display(Name = "Clave Departamento:")]
        public string DepartmentKey { get; set; }
        /// <summary>
        /// Nivel del empleado
        /// </summary>
        [Display(Name = "Nivel:")]
        public int Level { get; set; }
        /// <summary>
        /// Clave asiste
        /// </summary>
        [Display(Name = "Clave Asiste:")]
        public string AsisteKey { get; set; }
        /// <summary>
        /// Direccion del empleado
        /// </summary>
        [Display(Name = "Domicilio:")]
        public string Address { get; set; }
        /// <summary>
        /// Estado natal del empleado
        /// </summary>
        [Display(Name = "Estado:")]
        public string State { get; set; }
        /// <summary>
        /// Ciudad natal del empleado
        /// </summary>
        [Display(Name = "Ciudad:")]
        public string City { get; set; }
        /// <summary>
        /// Telefono de casa del empleado
        /// </summary>
        [Display(Name = "Telefono Casa:")]
        public string PhoneHouse { get; set; }
        /// <summary>
        /// Telefono celular del empleado
        /// </summary>
        [Display(Name = "Telefono Celular:")]
        public string CelularPhone { get; set; }
        /// <summary>
        /// Correo electronico del empleado
        /// </summary>
        [Display(Name = "Correo Electronico:")]
        public string Email { get; set; }

        /// <summary>
        /// Nombre del banco
        /// </summary>
        [Display(Name = "Nombre Banco:")]
        public string Bank { get; set; }
        /// <summary>
        /// Sucursal del banco
        /// </summary>
        [Display(Name = "Sucursal:")]
        public string BranchOffice { get; set; }
        /// <summary>
        /// Numero de cuenta
        /// </summary>
        [Display(Name = "Número Cuenta:")]
        public string AccountNumber { get; set; }
        /// <summary>
        /// Clave interbancaria
        /// </summary>
        [Display(Name = "Clave Interbancaria:")]
        public string ClaveInterbancaria { get; set; }
        /// <summary>
        /// Comenatrios generales
        /// </summary>
        [Display(Name = "Observaciones:")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        [Display(Name = "Empleado Activo:")]
        public bool ActiveEmployee { get; set; }

        public string DateInsert { get; set; }
        public string DateUpdate { get; set; }
        public string UserInsert { get; set; }
        public string UserUpdate { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.ViewModels
{
	public class StatesViewModel
	{
		public int TbStatesId { get; set; }
		public string NameSate { get; set; }

		public string DateInsert { get; set; }
		public string DateUpdate { get; set; }
		public string UserInsert { get; set; }
		public string UserUpdate { get; set; }
	}
}
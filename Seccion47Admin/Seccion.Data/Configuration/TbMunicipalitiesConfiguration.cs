﻿using Seccion.Model.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seccion.Data.Configuration
{
    public class TbMunicipalitiesConfiguration : BaseEntityTypeConfiguration<TbMunicipalities>
    {
        public TbMunicipalitiesConfiguration()
        {
            ToTable(nameof(TbMunicipalities));
            HasKey(x => x.TbMunicipalitiesId);
            Property(x => x.TbMunicipalitiesId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
            HasRequired(x => x.States).WithMany(x => x.Municipalities).HasForeignKey(x => x.TbStatesId).WillCascadeOnDelete(false);
            Property(x => x.NameMunicipalities).HasMaxLength(100);
            //base.Configure();
        }
    }
}

﻿using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Configuration
{
	public class TbFuneralExpensesConfiguration : BaseEntityTypeConfiguration<TbFuneralExpenses>
	{
		public TbFuneralExpensesConfiguration()
		{
			ToTable(nameof(TbFuneralExpenses));
			HasKey(x => x.TbFuneralExpensesId);
			Property(x => x.TbFuneralExpensesId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
			HasRequired(x => x.Employee).WithMany(x => x.FuneralExpenses).HasForeignKey(x => x.TbEmployeeId).WillCascadeOnDelete(false);
			Property(x => x.OrderNumberGF).HasMaxLength(10);
			Property(x => x.DateCreate).IsRequired();
			Property(x => x.ServiceConcept).HasMaxLength(200);
			Property(x => x.PaymentType).HasMaxLength(50);
			Property(x => x.VoucherKey).HasMaxLength(50);
			Property(x => x.AvalName).HasMaxLength(100);
			Property(x => x.PaymentAmount).HasPrecision(18, 2);
			Property(x => x.PeriodsPayment).HasMaxLength(10);
			Property(x => x.AbonoCatorcenalMensual).HasPrecision(18, 2);
			Property(x => x.Comments).HasMaxLength(400);
		}
	}
}

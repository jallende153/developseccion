﻿using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Configuration
{
	public class TbVoucherConfiguration :  BaseEntityTypeConfiguration<TbVoucher>
	{
		public TbVoucherConfiguration()
		{
			ToTable(nameof(TbVoucher));
			HasKey(x => x.TbVoucherId);
			Property(x => x.TbVoucherId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
			HasRequired(x => x.FuneralExpenses).WithMany(x => x.Vouchers).HasForeignKey(x => x.TbFuneralExpensesId).WillCascadeOnDelete(false);
			Property(x => x.NameVoucher).HasMaxLength(100);
		}
	}
}

﻿using System.Linq;
using System.Threading.Tasks;

namespace Seccion.Data.Infrastructure
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly IDbFactory _dbFactory;
        private StoreEntities _dbContext;

        public UnitOfWork(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        private StoreEntities DbContext
        {
            get { return _dbContext ?? (_dbContext = _dbFactory.Init() );}
        }

        public async Task Commit()
        {
            await DbContext.Commit();
        }

    }
}

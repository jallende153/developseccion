﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Seccion.Model.IdentityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Infrastructure
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole, int>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, int> roleStore)
            : base(roleStore) 
        { 
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            //var manager = new ApplicationRoleManager( new RoleStore<IdentityRole>(context.Get<StoreEntities>()));
            var manager = new ApplicationRoleManager(new ApplicationRoleStore(context.Get<StoreEntities>()));
            return manager;
        }

        public IEnumerable<ApplicationRole> GetRolesByUser(ApplicationUser user)
        {
            return Roles.Where(e => e.Users.Any(y => y.UserId == user.Id)).ToList();
        }
    }
}

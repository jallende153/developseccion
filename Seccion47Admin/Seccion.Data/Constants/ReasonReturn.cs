﻿namespace Seccion.Data.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class ReasonReturn
    {
        public const string ClassificationQuality = "Classificação da Qualidade";
        public const string EndExpatriation = "Fim da expatriação";
        public const string EndContract = "Fim do contrato (Trabalho)";
        public const string ChangeFunction = "Mudança de função";
        public const string ExchangewithoutPurchaseOption = "Troca sem opção de compra";
        public const string RelocatedVehicle = "Veículo será realocado";
        public const string SinisterVehicle = "Veículo sinistrado – Perda Total";
        public const string SaleManager = "Venda para gestor";
        public const string SeminewSale = "Venda Seminovo – Concessionária";
        public const string VIPSale = "Venda VIP";

        public static List<T> GetAllPublicConstantValues<T>(this Type type)
        {
            return type
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(T))
                .Select(x => (T)x.GetRawConstantValue())
                .ToList();
        }
    }
}

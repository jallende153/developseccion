﻿using Microsoft.AspNet.Identity;
using Seccion.Data.Enum;
using Seccion.Data.Infrastructure;
using Seccion.Data.Repositories;
using Seccion.Model.IdentityModel;
using Seccion.Service.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Seccion.Service.Services
{
	public class AccountUserService : IAccountUserService
	{
		private readonly IUnitOfWork unitOfWork;
		private readonly IAccountUserRepository _accountUserRepository;
		
		public AccountUserService(IUnitOfWork unitOfWork,IAccountUserRepository accountUserRepository)
		{
			this.unitOfWork = unitOfWork;
			_accountUserRepository = accountUserRepository;			
		}

		public async Task<IdentityResult> AccountUserCreate(ApplicationUser user, string pass)
		{
			return await _accountUserRepository.AccountUserCreate(user, pass);
		}

		public void AccountUserUpdate(ApplicationUser user)
		{
			_accountUserRepository.Update(user);
		}

		public async Task<IdentityResult> AddRole(int userId, string role)
		{
			return await _accountUserRepository.AddRole(userId, role);
		}

		public async Task<ClaimsIdentity> CreateUserIdentity(ApplicationUser appUser)
		{
			return await _accountUserRepository.CreateUserIdentity(appUser);
		}

		public async Task<string> EmailConfirmationToken(ApplicationUser appUser)
		{
			return await _accountUserRepository.EmailConfirmationToken(appUser);
		}

		public async Task<string> GetRoleById(int roleId)
		{
			return await _accountUserRepository.GetRoleById(roleId);
		}

		public async Task<IList<string>> GetRoleByUserId(int userId)
		{
			return await _accountUserRepository.GetRoleByUserId(userId);
		}

		public async Task<IdentityResult> DeleteRoleByUserId(int userId, string role)
		{
			return await _accountUserRepository.DeleteRoleByUserId(userId, role);
		}

		public List<Tuple<int, string>> GetRoles()
		{
			return _accountUserRepository.GetRoles();
		}

		public async Task<ApplicationUser> GetUserById(int id)
		{
			return await _accountUserRepository.GetUserById(id);
		}

		public async Task<ApplicationUser> GetUserByIdForLogin(string userNameOrEmail)
		{
			return await _accountUserRepository.GetUserByIdForLogin(userNameOrEmail);
		}

		public Task<IEnumerable<ApplicationUser>> GetUserList()
		{

			return _accountUserRepository.GetUserList();
		}

		public async Task<SignInStatusUser> SignInUser(string userName, string password)
		{
			return await _accountUserRepository.SignInUser(userName,  password);
		}
		public void SaveUser()
		{
			unitOfWork.Commit();
		}

		public async Task<IdentityResult> ChangePasswordUser(int userId, string currentPassword, string newPassword)
		{
			return await _accountUserRepository.ChangePasswordUser(userId, currentPassword, newPassword);
		}
	}
}

﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.IdentityModel
{
     public class ApplicationUserClaim : IdentityUserClaim<int>
    {
        public ApplicationUser User { get; set; }
    }
}

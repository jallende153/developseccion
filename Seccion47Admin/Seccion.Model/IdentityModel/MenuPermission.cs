﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.IdentityModel
{
    /// <summary>
    /// La clase MenuPermission está destinada a señalar un elemento del menú y un rol a lo que el usuario puede y no puede hacer.
    /// </summary>
    public class MenuPermission : BaseEntityModel
    {
        [Key,Column(Order = 1)]
        public virtual int RoleMenuId { get; set; }

        [Key, Column(Order = 2)]
        public virtual int PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
        public virtual ApplicationRoleMenu RoleMenu { get; set; }
    }
}

﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.IdentityModel
{
    public class ApplicationRole : IdentityRole<int, ApplicationUserRole>//, IRole<int>
    {
        public ApplicationRole() { }
        public ApplicationRole(string name)
            : this()
        {
            this.Name = name;
            MenuItems = new HashSet<ApplicationRoleMenu>();
        }
        /// <summary>
        /// Propiedad de Collecion de Elementos de l Menu
        /// </summary>
        public ICollection<ApplicationRoleMenu> MenuItems { get; set; }
    }
}

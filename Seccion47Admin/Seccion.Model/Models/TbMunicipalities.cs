﻿using System;
using System.Collections.Generic;

namespace Seccion.Model.Models
{
    public class TbMunicipalities : BaseEntityModel
    {
        public int TbMunicipalitiesId { get; set; }
        public int TbStatesId { get; set; }
        public string NameMunicipalities { get; set; }

        public virtual TbStates States { get; set; }
        public virtual ICollection<TbEmployee> Employee { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seccion.Model.Models
{
    /// <summary>
    /// Clase para usar y crear empleados(tabla)
    /// </summary>
    public class TbEmployee : BaseEntityModel
    {
        /// <summary>
        /// ID (PK) de la clase tabla en Base de datos
        /// </summary>
        public int TbEmployeeId { get; set; }
        /// <summary>
        /// ID(FK) relacion con la tabla Estados
        /// </summary>
        public int TbStatesId { get; set; }
        /// <summary>
        /// ID(FK) relacion con la tabla Municipios
        /// </summary>
        public int TbMunicipalitiesId { get; set; }
        /// <summary>
        /// Numero de ficha
        /// </summary>
        public string NumberFicha { get; set; }
        /// <summary>
        /// Nombre del empleado
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Apelido parterno del empleado
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Apelido materno del empleado
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// RFC del empleado
        /// </summary>
        public string RFC { get; set; }
        /// <summary>
        /// Situacion contractual del empleado
        /// </summary>
        public string ContractualSituation{ get; set; }
        /// <summary>
        /// Ubicacion laboral del empleado
        /// </summary>
        public string WorkLocation { get; set; }
        /// <summary>
        /// Rol del empleado
        /// </summary>
        public string Rol { get; set; }
        /// <summary>
        /// Region del empleado
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// Clave de departemento del empleado
        /// </summary>
        public string DepartmentKey { get; set; }
        /// <summary>
        /// Nivel del empleado
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// Clave asiste
        /// </summary>
        public string AsisteKey { get; set; }
        /// <summary>
        /// Direccion del empleado
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Estado natal del empleado
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Ciudad natal del empleado
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Telefono de casa del empleado
        /// </summary>
        public string PhoneHouse { get; set; }
        /// <summary>
        /// Telefono celular del empleado
        /// </summary>
        public string CelularPhone { get; set; }
        /// <summary>
        /// Correo electronico del empleado
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Fecha de nacimiento
        /// </summary>
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// Nombre del banco
        /// </summary>
        public string Bank { get; set; }
        /// <summary>
        /// Sucursal del banco
        /// </summary>
        public string BranchOffice { get; set; }
        /// <summary>
        /// Numero de cuenta
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Clave interbancaria
        /// </summary>
        public string ClaveInterbancaria { get; set; }
        /// <summary>
        /// Comenatrios generales
        /// </summary>
        public string Comments { get; set; }
        public bool ActiveEmployee { get; set; }

        public virtual TbStates States { get; set; }
        public virtual TbMunicipalities Municipalities { get; set; }
        //public virtual ICollection<GastosFunerarios> GastosFunerarios { get; set; }

        public virtual ICollection<TbRequest> Request { get; set; }
        public virtual ICollection<TbFuneralExpenses> FuneralExpenses { get; set; }
        /// <summary>
        /// Retonar el nombre completo del Empleado mas la Ficha.
        /// </summary>
        [NotMapped]
        public string EmployeeNameComplet {
            get
            {
                return Name + " " + FirstName + " " + LastName + " (" + NumberFicha + ")";
            }
        }
    }
}

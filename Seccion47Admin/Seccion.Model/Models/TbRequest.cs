﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seccion.Model.Models
{
    /// <summary>
    /// Clase (tabla) de entidades para la solicitud
    /// </summary>
    public class TbRequest : BaseEntityModel
    {
        /// <summary>
        /// PK de la tabla
        /// </summary>
        public int TbRequestId { get; set; }
        /// <summary>
        /// FK con PK empleados
        /// </summary>
        public int TbEmployeeId { get; set; }
        /// <summary>
        /// Fecha de creacion de la solicitud
        /// </summary>
        public DateTime DateCreateRequest { get; set; }
        /// <summary>
        /// Numero de la solicitud
        /// </summary>
        public string RequestNumber { get; set; }
        /// <summary>
        /// Monto a pagar
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// Peridos de pago
        /// </summary>
        public string PaymentsPeriod { get; set; }
        /// <summary>
        /// Tasa de descuento
        /// </summary>
        public decimal DiscountRate { get; set; }
        /// <summary>
        /// Gastos de administracion
        /// </summary>
        public decimal AdministrationExpenses { get; set; }
        /// <summary>
        /// Resultado de gastos de administracion
        /// </summary>
        public decimal AdministrationExpensesResult { get; set; }
        /// <summary>
        /// Meses adicionales
        /// </summary>
        public string AdditionalMonths { get; set; }
        /// <summary>
        /// Total a pagar
        /// </summary>
        public decimal TotalPay { get; set; }
        /// <summary>
        /// Abono catorcenal o mensual
        /// </summary>
        public decimal AbonoCatorcenalMensual { get; set; }
        /// <summary>
        /// Catorcena a liquidar
        /// </summary>
        public string CatorcenaLiquidate { get; set; }
        /// <summary>
        /// Seccion a la que pertenece
        /// </summary>
        public string BelongingSection { get; set; }
        /// <summary>
        /// Fecha de proxima subida
        /// </summary>
        public DateTime? NextRise { get; set; }
        /// <summary>
        /// Nombre del Aval
        /// </summary>
        public string AvalName { get; set; }
        /// <summary>
        /// Clave del Aval
        /// </summary>
        public string AvalKey { get; set; }
        /// <summary>
        /// Adeudo del Aval
        /// </summary>
        public decimal DebitAval { get; set; }
        /// <summary>
        /// Comentarios generales
        /// </summary>
        public string Comments { get; set; }
        /// <summary>
        /// Estatus de la solicitud
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// Unicamente para obtener el nombre del Empleado
        /// </summary>
        [NotMapped]
        public string EmployeeName 
        {
			get
			{
                return string.Join(" ", Employee.Name, Employee.FirstName, Employee.LastName);
			}
        }

        public virtual TbEmployee Employee { get; set; }
    }
}

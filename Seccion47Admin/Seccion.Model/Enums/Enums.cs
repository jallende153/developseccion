﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Seccion.Model.Enums
{
    class Enums
    {
    }

    
    public enum RequestTypes
    {
        [Display(Name = "Function")]
        Function =0,
        [Display(Name = "Location")]
        Location=1,
        [Display(Name = "Department")]
        Department=2,
        [Display(Name = "NissanToAll")]
        NissanToAll=3,
        [Display(Name = "Pool")]
        Pool = 4
    }

    public enum VehicleStatus
    {
        [Display(Name = "Disponível")]
        [Description("Disponível")] //tambien Activo
        DISPONIBLE = 0,

        [Display(Name = "Atribuindo")]
        [Description("No processo de atribuição")]
        ASIGNANDO = 1,

        //[Display(Name = "Atribuído")]
        [Display(Name = "O carro  é Atribuído")]
        [Description("O carro  é Atribuído")]
        ASIGNADO = 2,

        [Display(Name = "Devolvido")]
        [Description("O carro foi devolvido")]
        DEVUELTO = 3,

        [Display(Name = "Vendido")]
        [Description("O carro foi vendido")]
        VENDIDO = 4,

        [Display(Name = "Scrap")]
        [Description("Scrap")]
        SCRAP = 5,

        [Display(Name = "Emprestado")]
        [Description("Empresatado")]
        EMPRESTADO = 6,

        [Display(Name = "Recompra")]
        [Description("Recompra")]
        RECOMPRA = 7,

        [Display(Name = "Atribuição Externa")]
        [Description("Atribuição Externa")]
        FueraDeSistema = 8,

        [Display(Name = "Pool")]
        [Description("Pool")]
        Pool = 9,

        [Display(Name = "Compra")]
        [Description("Processo de solicitação de compra")]
        Compra = 10,

        [Display(Name = "Temporário")]
        [Description("Atribuição Temporária")]
        Temporario = 11,

        [Display(Name = "Roubado")]
        [Description("O carro foi roubado")]
        ROBADO = 12,

        [Display(Name = "Devolucion")]
        [Description("Em processo de devolução")]
        PROCESODEVOLUCION = 13
    }

    public enum ApprovedType
    {
        ROL =1,
        USUARIO = 2
    }

    public enum TypeMaintenance
    {

        Reparacion = 1,
        Revision = 2
    }

    public enum StepStatus
    {
        /// <summary>
        /// Pool
        /// </summary>
        SOLICITADO = 2
    }

    public enum IdentityRoles
    {

        Administrador,
        Frota
    }

    public enum RankGroups
    {
        FLOTA,
        FROTA,
        ANALISTAS,
        CONSULTOR,
        DIRETOR,
        GERENTE,
        MASTER,
        MENSALISTA,
        SUPERVISOR
    }

    public enum TypeAction
    {
        Create,
        Edit
    }

    public class CivilStatus
    {
        public const string Single = "Solteiro";
        public const string Married = "Casado";
        public const string FreeUnion = "União livre";
        public const string Separated = "Separado";
        public const string Divorced = "Divorciado";
        public const string Widowed = "Viúvo";
    }

    public enum Guilty
    {
        [Display(Name = "COLABORADOR/CONDUTOR")]
        [Description("COLABORADOR/CONDUTOR")]
        Collaborator,
        [Display(Name = "TERCEIRO")]
        [Description("TERCEIRO")]
        Third,
        [Display(Name = "OUTROS")]
        [Description("OUTROS")]
        Others
    }

    public enum SinisterStatus
    {
        [Description("Pendente")]
        Pending,
        [Description("Aprovado")]
        Approved,
        [Description("Recusado")]
        Declined
    }
}
